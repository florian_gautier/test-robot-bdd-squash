# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_3_SETUP}	Get Variable Value	${TEST 3 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_3_SETUP is not None	${__TEST_3_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_3_TEARDOWN}	Get Variable Value	${TEST 3 TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	Run Keyword If	$__TEST_3_TEARDOWN is not None	${__TEST_3_TEARDOWN}

*** Test Cases ***
Fail Dummy
	[Setup]	Test Setup

	Given some state
	When I do something
	Then I get an error

	[Teardown]	Test Teardown